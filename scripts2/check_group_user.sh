#!/bin/bash

#This script checks if a certain group and user exists


os=$(less /etc/group | grep "devops"| cut -c 1-6)
user=$(awk -F: '{ print $1 }' /etc/passwd | grep "engineer")

#Check group
if [ "$os" == "devops" ]; then
	echo "DevOps group exists!"
else
	sudo groupadd devops
fi


#Check user
if [ "$user" == "engineer" ]; then
	echo "User engineer already exists!"
else
	sudo useradd engineer
fi
