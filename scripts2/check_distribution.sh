#!/bin/bash

#This script checks the linux distribution, makes some updates and install some apps


# Extract distribution name

var=$(grep '^ID_LIKE' /etc/os-release | cut -c 9-14)


# Check name and install dependencies
if('$var' == 'debian');then
	echo "This is Debian"
	sudo apt-get update
	sudo apt-get upgrade 
	sudo apt-get install git
	sudo apt-get install vim 
else
	echo "This is Centos"
	sudo yum check-update
	sudo yum update
	sudo yum install git
	sudo yum install vim
fi

