#!/bin/bash

# This script takes the path and replaces all files with a certain extension with new one
# takes path old extension and new extension as arguments

path=$1
ext=$2
new_ext=$3

for f in $path/*$ext; do
    mv "$f" "${f%$ext}$new_ext"
done
